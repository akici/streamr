FROM node:12.7-alpine

COPY . .

# Serve the app
EXPOSE 4000

# Serve the app
CMD ["node", "dist/server/main.js"]
