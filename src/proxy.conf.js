const PROXY_CONFIG = {
  '/live-streams': {
    target: 'https://streamr.coolpixelz.com/live-streams',
    pathRewrite: {
      '^/live-streams': ''
    },
    secure: false,
    changeOrigin: true,
    logLevel: 'debug'
  }
};

module.exports = PROXY_CONFIG;
