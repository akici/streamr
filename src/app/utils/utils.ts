export class Utils {
  static getImageUrl(url: string): string {
    return url.replace('{width}', '640').replace('{height}', '360');
  }
}
