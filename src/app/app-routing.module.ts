import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PageComponent} from './core/components/page/page.component';
import {LiveStreamDetailComponent} from './modules/live-stream-detail/components/live-stream-detail/live-stream-detail.component';
import {LiveStreamsComponent} from './modules/live-streams/components/live-streams/live-streams.component';

const routes: Routes = [
  {
    path: '',
    component: PageComponent,
    children: [
      {path: '', redirectTo: 'catalog', pathMatch: 'full'},
      {path: 'catalog', component: LiveStreamsComponent},
      {path: 'channel/:id', component: LiveStreamDetailComponent},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabled'
})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
