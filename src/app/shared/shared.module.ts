import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatTabsModule} from '@angular/material/tabs';

export const MATERIAL_COMPONENTS = [MatButtonModule, MatIconModule, MatToolbarModule, MatTabsModule];

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  exports: [
    ...MATERIAL_COMPONENTS
  ]
})
export class SharedModule { }
