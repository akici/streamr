import {LiveStream, LiveStreamData} from '../models/live-stream';
import {Action, Selector, State, StateContext} from '@ngxs/store';
import {LiveStreamService} from '../services/live-stream.service';
import {map, tap} from 'rxjs/operators';
import {GetLiveStreams} from './live-stream.actions';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';

export class LiveStreamStateModel {
  streams: LiveStream[] = [];
  loaded = false;
}

@State<LiveStreamStateModel>({
  name: 'liveStreams',
  defaults:  new LiveStreamStateModel()
})
@Injectable()
export class LiveStreamState {
  constructor(private liveStreamService: LiveStreamService) {
  }

  @Selector()
  static getLiveStreamsList(state: LiveStreamStateModel): LiveStream[] {
    return state.streams;
  }

  @Selector()
  static areCoursesLoaded(state: LiveStreamStateModel): boolean {
    return state.loaded;
  }

  @Action(GetLiveStreams)
  fetchMovies({getState, setState}: StateContext<LiveStreamStateModel>): Observable<LiveStream[]> {
    return this.liveStreamService.getLiveStreamsData().pipe(
      map((res: LiveStreamData) => res.data),
      tap((streams: LiveStream[]) => {
        const state = getState();
        setState({
          ...state,
          streams,
          loaded: true
        });
      })
    );
  }
}
