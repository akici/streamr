import {Component, OnDestroy, OnInit} from '@angular/core';
import {Select, Store} from '@ngxs/store';
import {LiveStreamState} from '../../store/live-stream.state';
import {LiveStream} from '../../models/live-stream';
import {Observable, of, Subscription} from 'rxjs';
import {GetLiveStreams} from '../../store/live-stream.actions';
import {tap} from 'rxjs/operators';

@Component({
  selector: 'app-live-streams',
  templateUrl: './live-streams.component.html',
  styleUrls: ['./live-streams.component.scss']
})
export class LiveStreamsComponent implements OnInit, OnDestroy {
  @Select(LiveStreamState.getLiveStreamsList) liveStreams$: Observable<LiveStream[]>;

  @Select(LiveStreamState.areCoursesLoaded) streamsLoaded$: Observable<boolean>;

  isStreamsLoadedSubscription = new Subscription();

  constructor(private store: Store) { }

  ngOnInit(): void {
    this.isStreamsLoadedSubscription = this.streamsLoaded$.pipe(
      tap((loaded) => {
        if (!loaded) {
          this.store.dispatch(new GetLiveStreams());
        }
      })
    ).subscribe(value => {
      console.log(value);
    });
  }

  ngOnDestroy(): void {
    this.isStreamsLoadedSubscription.unsubscribe();
  }
}
