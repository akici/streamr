import {Component, Input, OnInit} from '@angular/core';
import {LiveStream} from '../../models/live-stream';
import {Utils} from '../../../../utils/utils';

@Component({
  selector: 'app-live-stream-item',
  templateUrl: './live-stream-item.component.html',
  styleUrls: ['./live-stream-item.component.scss']
})
export class LiveStreamItemComponent implements OnInit {
  @Input() liveStream: LiveStream;
  image: string;
  link: string;
  constructor() { }

  ngOnInit(): void {
    this.image = Utils.getImageUrl(this.liveStream.thumbnail_url);
    this.link = `/channel/${this.liveStream.user_name.toLowerCase()}`;
  }

}
