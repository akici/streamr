import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LiveStreamItemComponent } from './live-stream-item.component';

describe('LiveStreamItemComponent', () => {
  let component: LiveStreamItemComponent;
  let fixture: ComponentFixture<LiveStreamItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LiveStreamItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LiveStreamItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
