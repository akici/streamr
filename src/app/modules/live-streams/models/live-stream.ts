export interface LiveStream {
  id: string;
  user_name: string;
  game_id: string;
  game_name: string;
  title: string;
  thumbnail_url: string;
}

export interface LiveStreamData {
  data: LiveStream[];
}
