import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {LiveStreamData} from '../models/live-stream';

@Injectable({
  providedIn: 'root'
})
export class LiveStreamService {

  constructor(private httpClient: HttpClient) { }

  getLiveStreamsData(): Observable<LiveStreamData> {
    return this.httpClient.get<LiveStreamData>('/live-streams');
  }
}
