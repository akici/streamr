import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {LiveStreamService} from './services/live-stream.service';
import { LiveStreamsComponent } from './components/live-streams/live-streams.component';
import { LiveStreamItemComponent } from './components/live-stream-item/live-stream-item.component';
import {AppRoutingModule} from '../../app-routing.module';



@NgModule({
  declarations: [LiveStreamsComponent, LiveStreamItemComponent],
  imports: [
    CommonModule,
    AppRoutingModule,
  ],
  exports: [
    LiveStreamsComponent
  ],
  providers: [LiveStreamService]
})
export class LiveStreamsModule { }
