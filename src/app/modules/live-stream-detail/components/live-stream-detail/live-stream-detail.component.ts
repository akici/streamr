import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {map, shareReplay, takeWhile} from 'rxjs/operators';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-live-stream-detail',
  templateUrl: './live-stream-detail.component.html',
  styleUrls: ['./live-stream-detail.component.scss']
})
export class LiveStreamDetailComponent implements OnInit {
  private readonly PARAMETER_CHANNEL_ID = 'id';
  channelName$: Observable<string>;
  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.channelName$ = this.route.params.pipe(
      takeWhile(params => params[this.PARAMETER_CHANNEL_ID]),
      map(params => params[this.PARAMETER_CHANNEL_ID].toLowerCase()),
      shareReplay()
    );
  }

}
