import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LiveStreamDetailComponent } from './live-stream-detail.component';

describe('LiveStreamDetailComponent', () => {
  let component: LiveStreamDetailComponent;
  let fixture: ComponentFixture<LiveStreamDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LiveStreamDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LiveStreamDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
