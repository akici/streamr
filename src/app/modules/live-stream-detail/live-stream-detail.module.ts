import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {LiveStreamDetailComponent} from './components/live-stream-detail/live-stream-detail.component';
import {SharedModule} from '../../shared/shared.module';



@NgModule({
  declarations: [LiveStreamDetailComponent],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [LiveStreamDetailComponent]
})
export class LiveStreamDetailModule { }
